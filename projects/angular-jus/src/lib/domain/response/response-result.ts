class Response {
    message: String;
    success: boolean;
    date: Date;
    errors: string[];
}

export class ResponseResult<T> extends Response {
    result: T;
}

