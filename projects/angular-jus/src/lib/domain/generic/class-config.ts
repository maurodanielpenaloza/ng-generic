export class ClassConfig {
    entityName: String;
    orderFilter?: string[];
    idKeys: string[];
    deleteKeys?: string[];
    fieldConfig: FieldConfigHGeneric[];
}

export class FieldConfigHGeneric {
    key: string;
    display: String;
    isColumn: Boolean;
    isFilter: Boolean;
    typeFilter: FieldType;
    url?: string;
    options?: any[];
    optionsCheck?:any[];
  }

  export enum FieldType {
    inputText,
    inputNumber,
    inputPassword,
    radioButton,
    textarea,
    select,
    date,
    file,
    check
  }