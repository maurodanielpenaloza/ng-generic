import { EventEmitter } from '@angular/core'
export class ActionConfig {
    event: EventEmitter<any>;
    text: string;
    textResolver?: Function;
    colorResolver?:  Function;
}
