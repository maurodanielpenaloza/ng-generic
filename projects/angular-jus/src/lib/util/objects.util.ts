
export class ObjectUtil {
  //1653188400000
  static stringConstructor = 'test'.constructor;
  static arrayConstructor = [].constructor;
  static objectConstructor = {}.constructor;
  static dateConstructor = new Date().constructor;

  static getQueryMaprams(object: Object) {
    if (!object) {
      return;
    }
    let result = '';
    if (object.constructor === this.arrayConstructor) { // Valido que sea un json y no un array
        throw new Error('No se admiten arreglos debe ser un objeto JSON');
    }
    for (let key in object) {
      if (object.hasOwnProperty(key)) {
        const e = object[key];
        if (e !== false && !e) {
          continue;
        }
        // Si descubro que viene un objeto dentro de otro "aplano" el resultado y los atributos del hijo pasan como parametros
        if (e.constructor === this.objectConstructor) {
          result += this.getQueryMaprams(JSON.stringify(e));
          /* Si descubro que viene un arreglo paso todos los parametros con la misma
          key (Jackso reconoce de esa forma una lista y se mapea a una lista en backend)*/
        } else if (e.constructor === this.arrayConstructor) {
          e.forEach(subE => {
            if (subE.constructor === this.objectConstructor) {
              // TODO hacer que sea generico para un arbol de multiples niveles
            } else if (subE.constructor === this.arrayConstructor) {
              // TODO hacer que sea generico y volver a evaluar el arreglo sin limites recursivamente
            } else {
              result += key + '=' + subE + '&';
            }
          });
        } else if ( e.constructor === this.stringConstructor) {
          if (e !== '') {
            // reemplazo el espacio en blanco del string pero en angular 5 no haria falta
            result += key + '=' + e.replace(/ /g, '%20') + '&';
          }
        } else if (e.constructor === this.dateConstructor) {
          result += key + '=' + e.getTime() + '&';
        } else { // Si descubro que viene un nuemero directamente lo pongo con el valor que corresponde
          result += key + '=' + e + '&';
        }
      }
    }
    // En este paso elimino el & que quedo al final y ademas agrego el ? al comienzo del string
    return result !== '' ? result.slice(0, result.length - 1) : '';
  }

  static getValue(element: any, key: string) {
    if (key.indexOf(".") != -1) {
      let _key = key.split(".");
      let value = element[_key[0]];
      _key.shift();
      let _newKey = _key.join(".");
      return ObjectUtil.getValue(value, _newKey);
    } else {
      return element[key]
    }
  }
}
