import { Injectable } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Subject, Observable } from 'rxjs';
import { ErrorDialogComponent } from './dialog/error-dialog/error-dialog.component';
import { ConfirmDialogComponent } from './dialog/confirm-dialog/confirm-dialog.component';
import { InfoSnackBarComponent } from './snack-bar/info-snack-bar/info-snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor(public dialog: MatDialog, public snackBar: MatSnackBar) { }
  
  showError(errors: string[]): Observable<any> {
    let subject = new Subject<Boolean>();
    const dialogRef = this.dialog.open(ErrorDialogComponent,
    {
      data:
      {
        errors: errors
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      subject.next(result);
    });
    return subject;
  }   
  showAlertConfirmDialog(dialogMessage: string, dialogTitle: string): Observable<any> {
    if(!dialogTitle) {
      dialogTitle = "Alerta!";
    }

    let subject = new Subject<Boolean>();
    const dialogRef = this.dialog.open(ConfirmDialogComponent,
      {
        data:
        {
          message: dialogMessage,
          title: dialogTitle,
          alert: true
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      subject.next(result);
    });
    return subject;
  }
  showInfo(message: string) {
    this.snackBar.openFromComponent(InfoSnackBarComponent, {
      duration: 3000,
      horizontalPosition: 'left',
      verticalPosition: 'bottom',
      data: {
        message: message
      }
    });
  }

  showConfirmDialog(dialogMessage: String, dialogTitle?: String): Observable<Boolean> {
    if(!dialogTitle) {
      dialogTitle = "Confirmación Requerida";
    }

    let subject = new Subject<Boolean>();
    const dialogRef = this.dialog.open(ConfirmDialogComponent,
      {
        data:
        {
          message: dialogMessage,
          title: dialogTitle
        }
      });

    dialogRef.afterClosed().subscribe(result => {
      subject.next(result);
    });
    return subject;
  }

}
