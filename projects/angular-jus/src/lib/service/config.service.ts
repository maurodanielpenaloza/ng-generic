import { Injectable, Optional } from '@angular/core';
import { JusIntialConfig } from '../domain/jus-intial-config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(@Optional() config: JusIntialConfig) {
    if (config) { 
      config.urlApi = config.urlApi;
    }
  }
}

export const config: JusIntialConfig = {
  urlApi: '/api'
};
