import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadService {
  subjectGeneral: Subject<Boolean> = new Subject<Boolean>();
  subjectFormLoad: Subject<Boolean> = new Subject<Boolean>();
  hideLoadGeneralLoad(): any {
    this.subjectGeneral.next(false);
  }
  showLoadGeneralLoad(): void {
    this.subjectGeneral.next(true);
  }
  generalLoad(): Observable<Boolean> {
    return this.subjectGeneral;
  }
  hideLoadFormLoad(): any {
    this.subjectFormLoad.next(false);
  }
  showLoadFormLoad(): any {
    this.subjectFormLoad.next(true);
  }
  formLoad(): Observable<Boolean> {
    return this.subjectFormLoad;
  }

  public static evaluateLoad(isShow, _comp) {
    if(!isShow) {
      setTimeout(r => {
        _comp.activeLoad = isShow;
      }, 1000);
    } else {
      setTimeout(r => {
        _comp.activeLoad = isShow;
      }, 0);
      
    }
  }

  constructor() { }
}
