import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { ObjectUtil } from '../../util/objects.util';
import { LoadService } from '../load/load.service';
import { ResponseResult } from '../../domain/response/response-result';
import { PageResponse } from '../../domain/response/page-response';
import { config } from  '../config.service'
import { MessageService } from '../message/message.service';

export abstract class GenericService<E> {
  abstract url: string;

  constructor(private httpClient: HttpClient, public loadService: LoadService, public messageService: MessageService) {
  }
  
  add(e: E): Observable<ResponseResult<any>> {
    this.loadService.showLoadFormLoad();
    return this.httpClient.post<ResponseResult<any>>(this.makePath(), e)
      .pipe(
        map(resp => {
          if(resp.success) {
            this.messageService.showInfo("Se agrego correctamente");
          } else {
            let errors = resp.errors;
            this.messageService.showError(errors);
          }
          this.loadService.hideLoadFormLoad();
          return resp;
        })
      );
  }

  update(e: E): Observable<ResponseResult<any>> {
    this.loadService.showLoadFormLoad();
    return this.httpClient.put<ResponseResult<any>>(this.makePath(), e)
      .pipe(
        map(resp => {
          this.loadService.hideLoadFormLoad();
          return resp;
        })
      );
  }

  deactivate(id: number): Observable<any> {
    this.loadService.showLoadFormLoad();
    return this.httpClient.patch<ResponseResult<any>>(this.makePath() + '/deactivate/' + id, null);
  }

  activate(id: number): Observable<any> {
    this.loadService.showLoadFormLoad();
    return this.httpClient.patch<ResponseResult<any>>(this.makePath() + '/activate/' + id, null);
  }

  findAll({ p, s, showLoad = true }: { p: Number, s: Number, showLoad?: Boolean }): Observable<PageResponse<Array<E>>> {
    if (showLoad) {
      this.loadService.showLoadGeneralLoad();
    }
    return this.httpClient.get<ResponseResult<PageResponse<Array<E>>>>(this.makePath() + '?p=' + p + '&s=' + s)
      .pipe(
        map(resp => {
          if (showLoad) {
            this.loadService.hideLoadGeneralLoad();
          }
          return resp.result;
        })
      );
  }

  findAllByFilter(p: Number, s: Number, filter: any): Observable<PageResponse<Array<E>>> {
    this.loadService.showLoadGeneralLoad();
    return this.httpClient.get<ResponseResult<PageResponse<Array<E>>>>(this.makePath() + '/like' + '?p=' + p + '&s=' + s
      + '&' + ObjectUtil.getQueryMaprams(filter))
      .pipe(
        map(resp => {
          this.loadService.hideLoadGeneralLoad();
          return resp.result;
        })
      );
  }

  findById(id: Number): Observable<E> {
  this.loadService.showLoadGeneralLoad();
  return this.httpClient.get<ResponseResult<E>>(this.makePath() + "/" + id)
  .pipe(
    map(resp => {
      this.loadService.hideLoadGeneralLoad();
      return resp.result;
    })
    );
  }
  
  makePath(): string {
    return config.urlApi + this.url;
  }

  getUrl(){
    return this.url;
  }

  getHttpClient(): HttpClient {
    return this.httpClient;
  }

  public evaluateBusinessError(r: any): any {
    if(!r.success) {
      this.messageService.showError(r.errors);
    }
  }
}
