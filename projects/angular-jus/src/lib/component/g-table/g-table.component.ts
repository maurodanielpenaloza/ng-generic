import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material';
import { ActionConfig } from '../../domain/generic/action-config';
import { ObjectUtil } from '../../util/objects.util';
import { FieldConfigHGeneric, ClassConfig, FieldType } from '../../domain/generic/class-config';
import { MessageService } from '../../service/message/message.service';
import { GenericService } from '../../service/generic/generic.service';
import { ResponseResult } from '../../domain/response/response-result';
import { GFilterComponent } from '../g-filter/g-filter.component';

@Component({
  selector: 'g-table',
  templateUrl: './g-table.component.html',
  styleUrls: ['./g-table.component.css']
})
export class GTableComponent implements OnInit {
  pageSizeOptions: number[] = [5, 10, 20];
  _classConfig: ClassConfig;
  fieldType = FieldType;
  rowDef: String[] = [];
  entries: any[] = [];
  public elements: Array<any>;
  public page: Number;
  public size: Number;
  public length: Number = 0;
  public lastFilter: any = {};
  enableActions = false;
  showData;
  @ViewChild('paginator')
  paginator: MatPaginator;
  @Input()
  gFilter: GFilterComponent;
  @Input()
  set classConfig(classConfig: ClassConfig) {
    this._classConfig = classConfig;
    this.setEntries(this._classConfig.fieldConfig);
    this.setRowDef(this._classConfig.fieldConfig);
  }
  @Input()
  service: GenericService<any>;
  _actions: ActionConfig[];
  @Input()
  set actions(actions: ActionConfig[]) {
    this._actions = actions;
    this.evaluateActionRowDef();
  }
  _where: Map<string, any>;
  @Input()
  set where(w: Map<string, any>) {
    this._where = w;
    this.getByFilter(this.lastFilter);
  }

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    if (!this._where) {
      this.getByPage();
    } else {
      this.getByFilter(this._where)
    }
    if (this.gFilter) {
      this.gFilter.changeEmitter().subscribe(filterData => {
        //Asigno como ultimo filtro lo que viene como parametro para futuro filtro por cambio de pagina
        this.lastFilter = filterData;
        if(this.paginator.hasPreviousPage()) {
          //Si no esta en la primer pagina lo muevo a la primer pagina y se invoca sola la busqueda
          this.paginator.firstPage();
        } else {
          this.getByFilter(this.lastFilter);
        }
      })
    }
  }

  setRowDef(fieldConfig: FieldConfigHGeneric[]): any {
    this.rowDef = [];
    fieldConfig.forEach(
      field => {
        if (field.isColumn === true) {
          this.rowDef.push(
            field.key
          );
        }
      }
    );
    this.evaluateActionRowDef();
  }
  evaluateActionRowDef() {
    if (this.rowDef && this._actions && this._actions.length > 0) {
      this.rowDef.push('row-actions');
    }
  }
  setEntries(fieldConfig: FieldConfigHGeneric[]): any {
    this.entries = [];
    fieldConfig.forEach(
      field => {
        if (field.isColumn === true) {
          this.entries.push({
            key: field.key,
            display: field.display,
            type: field.typeFilter,
            options: field.options
          }
          );
        }
      }
    );
  }
  refresh() {
    this.getByFilter(this.lastFilter);
  }
  getByPage() {
    this.evaluatePageAndSize();
    this.service.findAll({ p: this.page, s: this.size, showLoad: true })
      .subscribe(resp => {
        this.elements = resp.elementos;
        this.length = resp.cantidad;
      }, (error: ResponseResult<any>) => {
        this.messageService.showError(error.errors);
      });
  }

  getByFilter(filter: any) {
    this.evaluatePageAndSize();
    this.lastFilter = filter;

    if (this._where && this._where.size > 0) {
      this._where.forEach((value: string, key: string) => {
        this.lastFilter[key] = value;
      });
    }
    console.info(this.lastFilter);
    this.service.findAllByFilter(this.page, this.size, filter)
      .subscribe(resp => {
        this.elements = resp.elementos;
        this.length = resp.cantidad;
      }, (error: ResponseResult<any>) => {
        this.messageService.showError(error.errors);
      });
  }
  private evaluatePageAndSize() {
    if (!this.page) {
      this.page = 0;
    }
    if (!this.size) {
      this.size = 10;
    }
  }
  changePage(ev: PageEvent): void {
    this.page = ev.pageIndex;
    this.size = ev.pageSize;
    this.getByFilter(this.lastFilter);
  }
  emitter(action: ActionConfig, element): void {
    if (action && action.event) {
      action.event.emit(element);
    }
  }
  existAction(): boolean {
    return this.actions && this.actions.length > 0;
  }

  getValue(element: any, key: string) {
    return ObjectUtil.getValue(element, key);
  }
}
