export class ListConfig<E> {
    id: any;
    title: string;
    data: E[];
    item: ItemConfig;
}
export class ItemConfig {
    title: string;
    description: string;
}
