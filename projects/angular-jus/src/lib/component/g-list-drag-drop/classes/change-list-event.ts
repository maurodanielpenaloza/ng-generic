import { ListConfig } from './list-config';

export class ChangeListEvent<E> {
    previus: ListConfig<E>;
    actual: ListConfig<E>;
    object: E;
}
