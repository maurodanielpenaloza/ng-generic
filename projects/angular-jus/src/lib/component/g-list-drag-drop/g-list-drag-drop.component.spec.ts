import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GListDragDropComponent } from './g-list-drag-drop.component';

describe('GListDragDropComponent', () => {
  let component: GListDragDropComponent;
  let fixture: ComponentFixture<GListDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GListDragDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GListDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
