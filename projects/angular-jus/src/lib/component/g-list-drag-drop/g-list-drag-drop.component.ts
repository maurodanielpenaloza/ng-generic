import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDropList } from '@angular/cdk/drag-drop';
import { ListConfig } from './classes/list-config';
import { ChangeListEvent } from './classes/change-list-event';
import { FieldType, ClassConfig, FieldConfigHGeneric } from '../../domain/generic/class-config';
import { ObjectUtil } from '../../util/objects.util';

@Component({
  selector: 'g-list-drag-drop',
  templateUrl: './g-list-drag-drop.component.html',
  styleUrls: ['./g-list-drag-drop.component.scss']
})
export class GListDragDropComponent implements OnInit {
  fieldType = FieldType;
  @Input()
  listConfigs: ListConfig<any>[];
  @Input()
  set classConfig(config: ClassConfig) {
    this._fieldConfigs = config.fieldConfig.filter(f=>f.isColumn);
  }
  _fieldConfigs: FieldConfigHGeneric[];
  _listOfCdkDD: CdkDropList[];
  _fieldCdkMap: Map<string, ListConfig<any>> = new Map();
  @Output()
  change: EventEmitter<ChangeListEvent<any>> = new EventEmitter();
  constructor() {
    this._listOfCdkDD = [];
  }

  ngOnInit() {
  }

  connectTo(todoList: CdkDropList, listConfig: any) {
    if (this._listOfCdkDD.indexOf(todoList) < 0) {
      this._listOfCdkDD.push(todoList);
      this._fieldCdkMap.set(todoList.id, listConfig);
    }
    return this._listOfCdkDD;
  }

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.change.emit({
        previus: this._fieldCdkMap.get(event.previousContainer.id),
        actual: this._fieldCdkMap.get(event.container.id),
        object: event.container.data[event.currentIndex]
      })
    }
  }
  getValue(element: any, key: string) {
    return ObjectUtil.getValue(element, key);
  }
}
