import { TestBed } from '@angular/core/testing';

import { SelectsDataService } from './selects-data.service';

describe('SelectsDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SelectsDataService = TestBed.get(SelectsDataService);
    expect(service).toBeTruthy();
  });
});
