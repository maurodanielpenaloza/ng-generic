import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ResponseResult } from '../../../domain/response/response-result';

@Injectable({
  providedIn: 'root'
})
export class SelectsDataService {

  constructor(private http: HttpClient) {
  }
  getData(url: string): Observable<[]> {
    return this.http.get<ResponseResult<[]>>(url)
    .pipe(
      map(resp => resp.result)
    );
  }
}
