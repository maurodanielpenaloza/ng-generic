import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GFilterComponent } from './g-filter.component';

describe('GFilterComponent', () => {
  let component: GFilterComponent;
  let fixture: ComponentFixture<GFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
