import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FieldType, FieldConfigHGeneric, ClassConfig } from '../../domain/generic/class-config';
import { MatDialog, MatSelect, MatDatepicker } from '@angular/material';
import { SelectsDataService } from './service/selects-data.service';

@Component({
  selector: 'g-filter',
  templateUrl: './g-filter.component.html',
  styleUrls: ['./g-filter.component.scss']
})
export class GFilterComponent implements OnInit {


  typeFilterHGeneric = FieldType;
  _fieldConfig: FieldConfigHGeneric[] = [];
  field: FieldConfigHGeneric;
  orderFields: String[];
  isFiltered: Boolean;
  entityName: String;
  @Output("change")
  _changeEmitter: EventEmitter<any>;
  @ViewChild('input') inputFilter: ElementRef;

  @Input()
  set fieldConfig(config: ClassConfig) {
    const _fields = config.fieldConfig;
    this.entityName = config.entityName;
    const _arr =  _fields.filter(c => c.isFilter);
    if (config.orderFilter && config.orderFilter.length > 0) {
      config.orderFilter.forEach(_field => {
        const field = _arr.find(_f => _f.key === _field);
        if (field) {
          this._fieldConfig.push(
            field
          );
        }
      });
    } else {
      this._fieldConfig = _arr;
    }

    this._fieldConfig.forEach(field => {
      if (field.typeFilter === FieldType.select || field.typeFilter === FieldType.check) {
        this.selectValues(field);
      }
    });
    if (this._fieldConfig && this._fieldConfig.length > 0) {
      this.field = this._fieldConfig[0];
    }
  }
  @Output() filter = new EventEmitter<any>();
  filtersValue: any = {};
  constructor(private selectDataService: SelectsDataService, public dialog: MatDialog) {
    this._changeEmitter =  new EventEmitter();
  }
  changeEmitter(): EventEmitter<any> {
    return this._changeEmitter;
  }

  ngOnInit() {
  }
  changeField() {
    this.filtersValue = {};
  }

  clean() {
    this.filtersValue = {};
    this.doFilter();
    this.isFiltered = false;
  }

  doFilter() {
    this._changeEmitter.emit(this.filtersValue);
    this.isFiltered = true;
  }

  selectValues(field: any) {
    if (field && !field.options) {
      if (field.url) {
        this.selectDataService.getData(field.url)
        .subscribe(options => {
          if (!field.options) {
            field.options = options;
          }
        }, error => {
          console.error(error);
        });
      }
    } else if (field.typeFilter === FieldType.check) {
      if (field.options && field.options.length > 2) {
        field.optionsCheck = [field.options[2], field.options[3]];
      } else if (field.options && field.options.length > 0) {
        field.optionsCheck = [field.options[0], field.options[1]];
      } else {
        field.optionsCheck = ['Si', 'No'];
      }
    } else {
      return;
    }
  }
  isCleanFilterValue() {
    return JSON.stringify(this.filtersValue) === '{}';
  }
  onEnterKeydown(event) {
    if (this.inputFilter instanceof MatSelect) {
      if (this.inputFilter.panelOpen) {
        return;
      }
    }
    this.doFilter();
  }
  opChange(event) {
    console.log(event);
  }
  onCloseDatePicker(event, field) {
    document.getElementById('inputDate').focus();
  }
  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.dialog.openDialogs.length > 0) {
      return;
    }
    if (this.inputFilter.nativeElement &&
        (this.inputFilter.nativeElement.type === 'text'
        || this.inputFilter.nativeElement.type === 'number')) {
      if (document.activeElement !== this.inputFilter.nativeElement) {
        const exp = /^[0-9a-z]{1}$/i;
        if (event.key.match(exp)) {
          this.inputFilter.nativeElement.focus();
          setTimeout( e => {
            if (this.filtersValue[this.field.key]) {
              this.filtersValue[this.field.key] = event.key;
            } else {
              this.filtersValue[this.field.key] = event.key;
            }
          }, 20);
        }
      }
    } else if (this.inputFilter instanceof MatSelect) {
        if (event.key === ' ') {
          this.inputFilter.focus();
          this.inputFilter.open();
        }
    } else if (this.inputFilter instanceof MatDatepicker) {
      if (event.key === ' ') {
        this.inputFilter.open();

      }
    }
  }
}
