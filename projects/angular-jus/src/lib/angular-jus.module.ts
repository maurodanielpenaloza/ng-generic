import { NgModule, Optional, SkipSelf } from '@angular/core';
import { GTableComponent } from './component/g-table/g-table.component';
import { GFilterComponent } from './component/g-filter/g-filter.component';
import { GListDragDropComponent } from './component/g-list-drag-drop/g-list-drag-drop.component';
import { ResponseResult } from './domain/response/response-result';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule, MatTableModule, MatPaginatorModule, MatInputModule, MatButtonModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatCheckboxModule, MatSnackBarModule, MatExpansionModule, MatDividerModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ConfirmDialogComponent } from './service/message/dialog/confirm-dialog/confirm-dialog.component';
import { InfoSnackBarComponent } from './service/message/snack-bar/info-snack-bar/info-snack-bar.component';
import { ErrorDialogComponent } from './service/message/dialog/error-dialog/error-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { ConfigService } from './service/config.service';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    GTableComponent,
    GFilterComponent,
    GListDragDropComponent,
    ConfirmDialogComponent,
    InfoSnackBarComponent,
    ErrorDialogComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FlexLayoutModule,
    MatDialogModule,
    DragDropModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDividerModule
  ],
  entryComponents: [
    ConfirmDialogComponent,
    InfoSnackBarComponent,
    ErrorDialogComponent
  ],
  exports: [GTableComponent, GFilterComponent, GListDragDropComponent]
})
export class AngularJusModule { 
  constructor (@Optional() @SkipSelf() parentModule: AngularJusModule) {
    if (parentModule) {
      throw new Error(
        'AngularJusModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: ConfigService): ModuleWithProviders {
    return {
      ngModule: AngularJusModule,
      providers: [
        {provide: ConfigService, useValue: config }
      ]
    };
  }
}
