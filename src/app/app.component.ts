import { Component, EventEmitter } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from './hero.service';
import { ClassConfig, ActionConfig, MessageService } from 'projects/angular-jus/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  classConfig: ClassConfig = Hero.getClassConfig();
  actions: ActionConfig[];
  
  constructor(public service: HeroService, private messageSerivce: MessageService) {
    let action = new ActionConfig();
    action.event = new EventEmitter();
    action.event.subscribe((hero) => this.detail(hero));
    action.text = 'Carteles';
    this.actions = [action];
  }
  detail(hero: Hero) {

    this.messageSerivce.showError(['ERROR ::: Nombre: '+ hero.name, 'ERROR ::: Id: '+hero.id]).subscribe(() => {
      this.messageSerivce.showConfirmDialog('Heroe: ' + hero.name +' <p>Id: ' + hero.id +'</p>', 'Dialogo INFO!! Detalle del Heroe').subscribe(() => {
        this.messageSerivce.showAlertConfirmDialog('Heroe: ' + hero.name +' <p>Id: ' + hero.id +'</p>', 'Alerta!!  Detalle del Heroe').subscribe(() => {
          this.messageSerivce.showInfo('Info:: Heroe: ' + hero.name );
        });
      });
    });
  }
}
