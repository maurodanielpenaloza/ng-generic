import { ClassConfig, FieldType } from 'projects/angular-jus/src/public-api';

export class Hero {
    id: number;
    name: string;

    public static getClassConfig(): ClassConfig {
        return {
            entityName: 'Heroes',
            idKeys: ['id'],
            fieldConfig: [
                {
                key: 'id',
                display: 'Identificador',
                isColumn: true,
                isFilter: false,
                typeFilter: FieldType.inputText
            },{
                key: 'name',
                display: 'Nombre',
                isColumn: true,
                isFilter: true,
                typeFilter: FieldType.inputText
            }]
        }
    }
}
