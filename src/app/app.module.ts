import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';

import { environment } from 'src/environments/environment.prod';
import { AngularJusModule } from 'projects/angular-jus/src/public-api';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    AngularJusModule.forRoot(
      {urlApi: environment.urlApi}
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
