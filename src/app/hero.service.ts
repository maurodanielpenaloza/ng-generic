import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HttpClient } from '@angular/common/http';
import { LoadService, MessageService, GenericService, ResponseResult, PageResponse, ObjectUtil } from 'projects/angular-jus/src/public-api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService extends GenericService<Hero> {
  url: string;

  constructor(httpClient: HttpClient, loadService: LoadService, messageService: MessageService) {
    super(httpClient, loadService, messageService);
    this.url = '/heroes';
  }

  /**
   * Se Sobreescribe este metodo porque Los servicios de InMemoryDB  y HttpClientInMemoryWebApiModule 
   * no se puede cambiar la estructura de su respuesta
   * @param p Pagina
   * @param s Tamaño
   */
  findAll({ p, s, showLoad = true }: { p: Number, s: Number, showLoad?: Boolean }): Observable<PageResponse<Array<Hero>>> {
    if (showLoad) {
      this.loadService.showLoadGeneralLoad();
    }
    return this.getHttpClient().get<Hero[]>(this.makePath() )
      .pipe(
        map(resp => {
          if (showLoad) {
            this.loadService.hideLoadGeneralLoad();
          }
          let page =  new PageResponse<Array<Hero>>();
          page.cantidad = resp.length;
          page.elementos = resp;
          return page;
        })
      );
  }

  /**
   * Se Sobreescribe este metodo porque Los servicios de InMemoryDB  y HttpClientInMemoryWebApiModule 
   * no se puede cambiar la estructura de su respuesta
   * @param p Pagina
   * @param s Tamaño
   * @param filter Filtro
   */
  findAllByFilter(p: Number, s: Number, filter: any): Observable<PageResponse<Array<Hero>>> {
    this.loadService.showLoadGeneralLoad();
    return this.getHttpClient().get<Hero[]>(this.makePath() + '?' + ObjectUtil.getQueryMaprams(filter))
      .pipe(
        map(resp => {
          this.loadService.hideLoadGeneralLoad();
          let page =  new PageResponse<Array<Hero>>();
          page.cantidad = resp.length;
          page.elementos = resp;
          return page;
        })
      );
  }

}
